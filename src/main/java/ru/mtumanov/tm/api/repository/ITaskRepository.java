package ru.mtumanov.tm.api.repository;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.model.Task;

public interface ITaskRepository {
    
    void add (Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

    void clear();

}
