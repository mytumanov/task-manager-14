package ru.mtumanov.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();
    
}
