package ru.mtumanov.tm.api.controller;

public interface IProjectTaskController {
    
    void bindTaskToProject();

    void unbindTaskFromProject();
}
